package dxc;
class Participant{
	public static int counter;
	public String RegistrationId;
	public String name;
	public long contactNumber;
	public String city;
	static
	{
	  counter=10000;
	  
	}

Participant(String name, long contactNumber, String city){
		this.name =  name;
		this.contactNumber =  contactNumber;
		this.city =  city;
		this.RegistrationId="D"+ ++counter;
		}
public static int getCounter() {
		return counter;
	}
	public static void setCounter(int counter) {
		Participant.counter = counter;
	}
	public String getRegistrationId() {
		return RegistrationId;
	}
	public void setRegistrationId(String registrationId) {
		this.RegistrationId = registrationId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
}
public class Tester4 {

	public static void main(String[] args) {
		
		Participant participant1 = new Participant("Franklin", 7656784323L, "Texas");
		Participant participant2 = new Participant("Merina", 7890423112L, "New York");
		//Create more objects and add them to the participants array for testing your code
		Participant[] participants = { participant1, participant2 };
		for (Participant participant : participants) {
		System.out.println("Hi "+participant.getName()+"! Your Registration id is "+participant.getRegistrationId());
				}

	}

}
