package dxc;

class Bill 
{
private static int counter;
private String paymentMode;
private String billId;
static
{
  counter=9000;	
}
public Bill(String paymentMode) {
	this.billId="B"+ ++counter;
	this.paymentMode = paymentMode;
	
}

public String getPaymentMode() {
	return paymentMode;
}

public void setPaymentMode(String paymentMode) {
	this.paymentMode = paymentMode;
}

public String getBillId() {
	return billId;
}

public void setBillId(String bilId) {
	this.billId = bilId;
}
}


public class Tester3 {

	public static void main(String[] args) {
		Bill bill1 = new Bill("DebitCard");
		 Bill bill2 = new Bill("PayPal");
		 
		 //Create more objects and add them to the bills array for testing your code
		 
		 Bill[] bills = { bill1, bill2 };
		 
		 for (Bill bill : bills) {
		 System.out.println("Bill Details");
		 System.out.println("Bill Id: " + bill.getBillId());
		 System.out.println("Payment method: " + bill.getPaymentMode());
		 System.out.println();
		 }

	}

}
